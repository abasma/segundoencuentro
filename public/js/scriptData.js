import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyBiwsR92J4kXf0y9bs6MHU34nSXbLe0ADI",
    authDomain: "accesoconsole.firebaseapp.com",
    databaseURL: "https://accesoconsole-default-rtdb.firebaseio.com/",
    projectId: "accesoconsole",
    storageBucket: "accesoconsole.appspot.com",
    messagingSenderId: "612653277044",
    appId: "1:612653277044:web:a2b2a116f59f0161d45621"
  };

const app = initializeApp(firebaseConfig);

// Get a reference to the database service
const database = getDatabase(app);

// Referencias al HTML
let textoRef = document.getElementById("textoBaseDatosId");
let botonRef = document.getElementById("buttonBaseDatosId");
let valorDataBaseRef = document.getElementById("valorDataBaseId");
let Entrada = document.getElementById("Entradatxt");


// Event Listeners
botonRef.addEventListener("click", cargarInformacion);

// Función de carga de información empleando el método set de la API de Firebase Realtime Database
function cargarInformacion(){
    let informacion = textoRef.value;

    // API Docs.: https://firebase.google.com/docs/reference/js/database.md?hl=en#set
    set(ref(database, 'deitabeis/'), {
        data: informacion
      });

    console.log("Carga de información correcta.");
}

// API Docs.: https://firebase.google.com/docs/reference/js/database.md?hl=en#onvalue
onValue(ref(database, 'deitabeis/'), (snapshot) => {
    const lectura = snapshot.val();

    console.log(lectura.data);

    valorDataBaseRef.innerHTML = lectura.data;
});

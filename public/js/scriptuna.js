import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyBiwsR92J4kXf0y9bs6MHU34nSXbLe0ADI",
    authDomain: "accesoconsole.firebaseapp.com",
    projectId: "accesoconsole",
    storageBucket: "accesoconsole.appspot.com",
    messagingSenderId: "612653277044",
    appId: "1:612653277044:web:a2b2a116f59f0161d45621"
  };

  const app = initializeApp(firebaseConfig);

var MailRef = document.getElementById("floatingInput")
var PasswordRef = document.getElementById("floatingPassword")
var RegisterRef = document.getElementById("floatingButton2")
var LogInRef = document.getElementById("floatingButton1")

RegisterRef.addEventListener("click", checking);
LogInRef.addEventListener("click", entrar);

const auth = getAuth();

function checking() {
createUserWithEmailAndPassword(auth, MailRef.value, PasswordRef.value)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;

    console.log("Creación de Usuario Completada")
    // ...
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;

    console.log("Ha ocurrido un error")
    console.log(errorCode)
    console.log(errorMessage)
    // ..
  });
}

function entrar() {
signInWithEmailAndPassword(auth, MailRef.value, PasswordRef.value)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    console.log("El mail ingresado fue " + "MailRef.value")
    console.log("La contraseña ingresada fue " + "PasswordRef.value")
    console.log("Has Entrado correctamente")

    window.location.href = '../pages/Principal.html';
    // ...
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
  });
}

